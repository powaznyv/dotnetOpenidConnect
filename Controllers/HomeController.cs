﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using TokenGenerator.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using Newtonsoft.Json;

namespace TokenGenerator.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        public IConfiguration Configuration { get; }

        public const string ClaimTypeScope = "http://schemas.microsoft.com/identity/claims/scope";
        public const string ClaimTypeAudience = "aud";
        public const string ClaimTypeAppId = "appid";
        public const string ClaimTypeAuthTime = "auth_time";

        public HomeController(ILogger<HomeController> logger, IConfiguration configuration)
        {
            _logger = logger;
            Configuration = configuration;
        }

        [Authorize]
        public async Task<IActionResult> Index()
        {
            // Configuration
            ViewBag.ClientId = Configuration["OpenIDConnect:ClientId"];
            ViewBag.SignInScheme = Configuration["OpenIDConnect:SignInScheme"];
            ViewBag.Authority = Configuration["OpenIDConnect:Authority"];
            ViewBag.RequireHttpsMetadata = bool.Parse(Configuration["OpenIDConnect:RequireHttpsMetadata"]);
            ViewBag.UsePkce =    Configuration["OpenIDConnect:UsePkce"];
            ViewBag.scopes = string.Join(", ", Configuration.GetSection("OpenIDConnect:Scopes").Get<List<string>>());
            ViewBag.SaveTokens = Configuration["OpenIDConnect:SaveTokens"];
            ViewBag.MetadataAddress = Configuration["OpenIDConnect:MetadataAddress"];
            
            // token
            string accessToken = await HttpContext.GetUserAccessTokenAsync();
            ViewBag.token = accessToken;

            // user
            ViewBag.Upn = User.FindFirst(ClaimTypes.Upn)?.Value;
            ViewBag.GivenName = User.FindFirst(ClaimTypes.GivenName)?.Value;
            ViewBag.SurName = User.FindFirst(ClaimTypes.Surname)?.Value;
            ViewBag.Email = User.FindFirst(ClaimTypes.Email)?.Value;
            ViewBag.Audience = User.FindFirst(ClaimTypeAudience)?.Value;
            ViewBag.Scope = User.FindFirst(ClaimTypeScope)?.Value;
            ViewBag.AppId = User.FindFirst(ClaimTypeAppId)?.Value;
            ViewBag.AuhTime = User.FindFirst(ClaimTypeAuthTime)?.Value;
            DecryptToken(accessToken);
            

            return View();
        }

        public async Task Logout()
        {
            await HttpContext.SignOutAsync("Cookies");
            await HttpContext.SignOutAsync("oidc");
        }
        
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        private string DecryptToken(String token) {

            var handler = new JwtSecurityTokenHandler();
            var jwtSecurityToken = handler.ReadJwtToken(token);

            ViewBag.Jwtheader = JsonConvert.SerializeObject(jwtSecurityToken.Header, Formatting.Indented).Replace("\n", "<br/>");
            ViewBag.JwtPayload = JsonConvert.SerializeObject(jwtSecurityToken.Payload, Formatting.Indented).Replace("\n", "<br/>");

            return jwtSecurityToken.ToString();
        }

    }
}
