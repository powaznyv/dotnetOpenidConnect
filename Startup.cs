using System.Collections.Generic;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;
using Microsoft.AspNetCore.HttpOverrides;

namespace TokenGenerator
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            
            // OpenID Connect
            services.AddAuthentication(options =>
            {
                options.DefaultScheme = "Cookies";
                options.DefaultChallengeScheme = "oidc";
            })
            .AddCookie("Cookies")
            .AddOpenIdConnect("oidc", options =>
            {
                options.SignInScheme = Configuration["OpenIDConnect:SignInScheme"];
                options.Authority = Configuration["OpenIDConnect:Authority"];
                options.RequireHttpsMetadata = bool.Parse(Configuration["OpenIDConnect:RequireHttpsMetadata"]);
                options.ResponseType = OpenIdConnectResponseType.Code;
                options.UsePkce = bool.Parse(Configuration["OpenIDConnect:UsePkce"]);
                options.Scope.Clear();
                List<string> scopes = Configuration.GetSection("OpenIDConnect:Scopes").Get<List<string>>();
                foreach (var scope in scopes)
                {
                   options.Scope.Add(scope); 
                }
                options.SaveTokens = bool.Parse(Configuration["OpenIDConnect:SaveTokens"]);
                options.MetadataAddress = Configuration["OpenIDConnect:MetadataAddress"];
                options.ClientId = Configuration["OpenIDConnect:ClientId"];
            });

            services.AddAccessTokenManagement();

            services.AddControllersWithViews();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();

            app.UseRouting();

            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
            });

            app.UseAuthentication();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
